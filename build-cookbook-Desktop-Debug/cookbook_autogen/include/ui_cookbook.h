/********************************************************************************
** Form generated from reading UI file 'cookbook.ui'
**
** Created by: Qt User Interface Compiler version 6.3.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_COOKBOOK_H
#define UI_COOKBOOK_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_CookBook
{
public:
    QWidget *centralwidget;
    QMenuBar *menubar;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *CookBook)
    {
        if (CookBook->objectName().isEmpty())
            CookBook->setObjectName(QString::fromUtf8("CookBook"));
        CookBook->resize(800, 600);
        centralwidget = new QWidget(CookBook);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        CookBook->setCentralWidget(centralwidget);
        menubar = new QMenuBar(CookBook);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        CookBook->setMenuBar(menubar);
        statusbar = new QStatusBar(CookBook);
        statusbar->setObjectName(QString::fromUtf8("statusbar"));
        CookBook->setStatusBar(statusbar);

        retranslateUi(CookBook);

        QMetaObject::connectSlotsByName(CookBook);
    } // setupUi

    void retranslateUi(QMainWindow *CookBook)
    {
        CookBook->setWindowTitle(QCoreApplication::translate("CookBook", "CookBook", nullptr));
    } // retranslateUi

};

namespace Ui {
    class CookBook: public Ui_CookBook {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_COOKBOOK_H
