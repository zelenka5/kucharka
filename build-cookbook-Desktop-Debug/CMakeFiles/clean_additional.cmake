# Additional clean files
cmake_minimum_required(VERSION 3.16)

if("${CONFIG}" STREQUAL "" OR "${CONFIG}" STREQUAL "Debug")
  file(REMOVE_RECURSE
  "CMakeFiles/cookbook_autogen.dir/AutogenUsed.txt"
  "CMakeFiles/cookbook_autogen.dir/ParseCache.txt"
  "cookbook_autogen"
  )
endif()
