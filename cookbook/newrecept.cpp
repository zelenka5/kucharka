#include "newrecept.h"

NewRecept::NewRecept(QWidget *parent) : QMainWindow(parent) {
    QWidget *w = new QWidget(parent);
    QVBoxLayout *layout = new QVBoxLayout();

    QLabel *lbl = new QLabel("Nový recept");
    lbl->setStyleSheet("color: green; font-size: 30px;");
    layout->addWidget(lbl, 0, Qt::AlignCenter);

    QWidget *new_recipe = new QWidget();
    QVBoxLayout *new_layout = new QVBoxLayout();

    QLabel *name_lbl = new QLabel("Název:");
    name = new QTextEdit();
    QLabel *img_lbl = new QLabel("Obrázek:");
    QPushButton *img_btn = new QPushButton("Vybrat obrázek...");
    img_mini = new QLabel();
    QLabel *ings_lbl = new QLabel("Ingredience:");
    ingredients = new QTextEdit();
    QLabel *instructions_lbl = new QLabel("Postup:");
    instructions = new QTextEdit();

    name_lbl->setStyleSheet("font-size: 20px;");
    img_lbl->setStyleSheet("font-size: 20px;");
    ings_lbl->setStyleSheet("font-size: 20px;");
    instructions_lbl->setStyleSheet("font-size: 20px;");
    img_btn->setStyleSheet("font-size: 15px;");
    name->setFixedSize(1150,60);

    new_layout->addWidget(name_lbl, 0, Qt::AlignCenter);
    new_layout->addWidget(name);
    new_layout->addWidget(img_lbl, 0, Qt::AlignCenter);
    new_layout->addWidget(img_btn, 0, Qt::AlignCenter);
    new_layout->addWidget(img_mini, 0, Qt::AlignCenter);
    new_layout->addWidget(ings_lbl, 0, Qt::AlignCenter);
    new_layout->addWidget(ingredients);
    new_layout->addWidget(instructions_lbl, 0, Qt::AlignCenter);
    new_layout->addWidget(instructions);

    QScrollArea *scroll = new QScrollArea();
    new_recipe->setLayout(new_layout);
    scroll->setWidget(new_recipe);

    layout->addWidget(scroll);

    QHBoxLayout *btn_layout = new QHBoxLayout();
    QPushButton *back_btn = new QPushButton("Vrátit se");
    QPushButton *save_btn = new QPushButton("Uložit");
    btn_layout->addWidget(back_btn);
    btn_layout->addWidget(save_btn);
    layout->addLayout(btn_layout);

    layout->setAlignment(Qt::AlignTop);
    w->setLayout(layout);
    setCentralWidget(w);

    connect(img_btn, &QPushButton::clicked, this, &NewRecept::selectFile);
    connect(back_btn, &QPushButton::clicked, this, &NewRecept::exitSlot);
    connect(save_btn, &QPushButton::clicked, this, &NewRecept::saveRecipe);
}

NewRecept::~NewRecept() {

}

void NewRecept::selectFile() {
    img_name = QFileDialog::getOpenFileName(nullptr, "Select an image", QDir::currentPath(), "Image Files (*.png *.jpg *.bmp)");
    img = new QImage(img_name);
    img_mini->setPixmap(QPixmap::fromImage(img->scaled(300,200, Qt::KeepAspectRatio)));
}

void NewRecept::exitSlot() {
    deleteText();
    emit NewRecept::backToMenu();
}

void NewRecept::saveRecipe() {
    QString rec_name = name->toPlainText();
    QString rec_ings = ingredients->toPlainText();
    QString rec_insts = instructions->toPlainText();

    QSqlQuery insert_recipe;
    insert_recipe.prepare("INSERT INTO recipes (name, ingredients, instructions) VALUES (:name, :ingredients, :instructions);");
    insert_recipe.bindValue(":name", rec_name);
    insert_recipe.bindValue(":ingredients", rec_ings);
    insert_recipe.bindValue(":instructions", rec_insts);
    insert_recipe.exec();

    img->save("images/" + rec_name + ".jpg");
    deleteText();
    exitSlot();

}

void NewRecept::deleteText() {
    name->clear();
    ingredients->clear();
    instructions->clear();
    img_mini->clear();
    img = new QImage();
}
