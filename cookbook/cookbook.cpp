#include "cookbook.h"

CookBook::CookBook(QWidget *parent) : QMainWindow(parent) {
    this->resize(1200,800);

    db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName("cookbook.db");
    db.open();

    QSqlQuery db_create("CREATE TABLE IF NOT EXISTS recipes ("
                        "name VARCHAR PRIMARY KEY,"
                        "ingredients TEXT,"
                        "instructions TEXT);");
    db_create.exec();

    main_menu = new QWidget;
    menu_layout = new QVBoxLayout;

    menu_lbl = new QLabel("MENU");
    menu_lbl->setStyleSheet("color: green; font-size: 50px;");
    menu_layout->addWidget(menu_lbl, 0, Qt::AlignCenter);

    recipes_btn = new QPushButton("Recepty");
    new_btn = new QPushButton("Nový recept");
    exit_btn = new QPushButton("Konec");
    recipes_btn->setStyleSheet("font-size: 30px;");
    new_btn->setStyleSheet("font-size: 30px;");
    exit_btn->setStyleSheet("font-size: 30px;");
    recipes_btn->setMinimumWidth(200);
    new_btn->setMinimumWidth(200);
    exit_btn->setMinimumWidth(200);

    menu_layout->addWidget(recipes_btn, 0, Qt::AlignCenter);
    menu_layout->addWidget(new_btn, 0, Qt::AlignCenter);
    menu_layout->addWidget(exit_btn, 0, Qt::AlignCenter);
    menu_layout->addSpacing(100);

    main_menu->setLayout(menu_layout);

    recipes = new Recipes();
    new_recept = new NewRecept();

    connect(recipes_btn, &QPushButton::released, this, &CookBook::openRecipes);
    connect(new_btn, &QPushButton::released, this, &CookBook::openNewRecept);
    connect(exit_btn, &QPushButton::clicked, this, &CookBook::exitSlot);
    connect(recipes, &Recipes::backToMenu, this, &CookBook::returnSlot);
    connect(new_recept, &NewRecept::backToMenu, this, &CookBook::returnSlot);

    stack = new QStackedWidget();
    stack->addWidget(main_menu);
    stack->addWidget(recipes);
    stack->addWidget(new_recept);

    setCentralWidget(stack);
}

CookBook::~CookBook() {
     db.close();
}

void CookBook::openRecipes() {
    stack->setCurrentWidget(recipes);
}

void CookBook::openNewRecept() {
    stack->setCurrentWidget(new_recept);
}

void CookBook::exitSlot() {
    CookBook::close();
}

void CookBook::returnSlot() {
    stack->setCurrentWidget(main_menu);

//    QSqlQuery db_select("SELECT * FROM recipes;");;
//    for (int i =0; db_select.next(); i++)  {
//        QString n = db_select.value(0).toString();
//        menu_layout->addWidget(new QLabel(n));
//        menu_layout->addWidget(new QLabel("DB SELECT"));

//    }
}
