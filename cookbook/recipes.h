#ifndef RECIPES_H
#define RECIPES_H

#include <QMainWindow>
#include <QWidget>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QDialog>
#include <QLabel>
#include <QPushButton>
#include <QScrollArea>
#include <QStackedWidget>
#include <QImage>
#include <QPixmap>

#include <QSqlDatabase>
#include <QSqlQuery>

#include "recipewindow.h"

class Recipes : public QMainWindow
{
    Q_OBJECT
public:
    Recipes(QWidget *parent = nullptr);
    ~Recipes();
    void prepareRecipes();

public slots:
    void exitSlot();
    void openRecipe(int idx);
    void returnSlot();

signals:
    void backToMenu();
    void selectRecipe(int i);

private:
    QVBoxLayout *layout;
    QWidget *w;
    QStackedWidget *stack;
    QVBoxLayout *recipes_list;
    std::vector<QPushButton*> all_recipes_btns;
    std::vector<RecipeWindow*>  all_recipes_inst;

};

#endif // RECIPES_H
