#ifndef COOKBOOK_H
#define COOKBOOK_H

#include <QMainWindow>
#include <QWidget>
#include <QStackedWidget>
#include <QVBoxLayout>

#include <QButtonGroup>
#include <QLabel>
#include <QPushButton>
#include <QStringList>
#include <QString>

#include <QSqlDatabase>
#include <QSqlQuery>

#include <QDebug>

#include "recipes.h"
#include "newrecept.h"

class CookBook : public QMainWindow
{
    Q_OBJECT

public:
    CookBook(QWidget *parent = nullptr);
    ~CookBook();

public slots:
    void openRecipes();
    void openNewRecept();
    void exitSlot();
    void returnSlot();

private:
    QSqlDatabase db;
    QWidget *main_menu;
    QVBoxLayout *menu_layout;
    QLabel *menu_lbl;
    QPushButton *recipes_btn;
    QPushButton *new_btn;
    QPushButton *exit_btn;
    Recipes *recipes;
    NewRecept *new_recept;

    QStackedWidget *stack;

};
#endif // COOKBOOK_H
