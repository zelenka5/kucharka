#include "recipewindow.h"

RecipeWindow::RecipeWindow(QWidget *parent, QString name, QString ings, QString inst, QImage *image) : QMainWindow(parent) {
    QWidget *w = new QWidget(parent);
    QVBoxLayout *layout = new QVBoxLayout();

    recipe_name = new QLabel();
    recipe_name->setStyleSheet("font-size: 30px;");
    layout->addWidget(recipe_name, 0, Qt::AlignCenter);
    layout->setAlignment(Qt::AlignTop);

    QScrollArea *scroll = new QScrollArea();
    scroll->setHorizontalScrollBarPolicy(Qt::ScrollBarPolicy(1));
    QWidget *recipe = new QWidget();
    QVBoxLayout *rec_layout = new QVBoxLayout();

    food_image = new QLabel();
    setRecipe(name, ings, inst,image);

    rec_layout->addWidget(food_image);
    QLabel *label_ing = new QLabel("Ingredience:");
    label_ing->setStyleSheet("font-size: 22px;");
    rec_layout->addWidget(label_ing);
    rec_layout->addWidget(ingredients);
    QLabel *label_inst = new QLabel("Postup:");
    label_inst->setStyleSheet("font-size: 22px;");
    rec_layout->addWidget(label_inst);
    rec_layout->addWidget(instructions);
    recipe->setLayout(rec_layout);

    scroll->setWidget(recipe);
    layout->addWidget(scroll);

    QPushButton *exit_btn = new QPushButton("Vrátit se");
    layout->addWidget(exit_btn);
    w->setLayout(layout);

    connect(exit_btn, &QPushButton::clicked, this, &RecipeWindow::exitSlot);

    setCentralWidget(w);
}

void RecipeWindow::exitSlot() {
    emit RecipeWindow::backToRecipes();
}

void RecipeWindow::setRecipe(QString name, QString ings, QString inst, QImage *image) {
    recipe_name->setText(name);
    ingredients = new QLabel(ings);
    ingredients->setStyleSheet("font-size: 18px;");
    instructions = new QLabel(inst);
    instructions->setStyleSheet("font-size: 18px;");
    food_image->setPixmap(QPixmap::fromImage(image->scaled(600,400, Qt::KeepAspectRatio)));
}


RecipeWindow::~RecipeWindow() {

}
