#ifndef RECIPEWINDOW_H
#define RECIPEWINDOW_H

#include <QMainWindow>
#include <QObject>
#include <QWidget>
#include <QVBoxLayout>
#include <QLayout>

#include <QLabel>
#include <QScrollArea>
#include <QPushButton>

#include <QImage>
#include <QPixmap>

class RecipeWindow : public QMainWindow
{
    Q_OBJECT
public:
    RecipeWindow(QWidget *parent = nullptr, QString name = "", QString ings = "", QString inst = "", QImage *image = new QImage());
    ~RecipeWindow();
    void setRecipe(QString name, QString ingredients, QString instructions, QImage *image);

public slots:
    void exitSlot();

signals:
    void backToRecipes();

private:
    QVBoxLayout *layout;
    QLabel *recipe_name;
    QLabel *food_image;
    QLabel *ingredients;
    QLabel *instructions;

};

#endif // RECIPEWINDOW_H
