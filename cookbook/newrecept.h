#ifndef NEWRECEPT_H
#define NEWRECEPT_H

#include <QMainWindow>
#include <QWidget>
#include <QStackedWidget>
#include <QVBoxLayout>
#include <QScrollArea>

#include <QButtonGroup>
#include <QLabel>
#include <QPushButton>
#include <QTextEdit>
#include <QFileDialog>
#include <QString>
#include <QPixmap>
#include <QImage>
#include <QSize>

#include <QSqlDatabase>
#include <QSqlQuery>

class NewRecept : public QMainWindow
{
    Q_OBJECT

public:
    NewRecept(QWidget *parent = nullptr);
    ~NewRecept();

public slots:
    void exitSlot();
    void selectFile();
    void saveRecipe();

signals:
    void backToMenu();

private:
    QTextEdit *name;
    QTextEdit *ingredients;
    QTextEdit *instructions;

    QString img_name;
    QLabel *img_mini;
    QImage *img;

    void deleteText();
};

#endif // NEWRECEPT_H
