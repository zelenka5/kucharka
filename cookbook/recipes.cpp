#include "recipes.h"

Recipes::Recipes(QWidget *parent) : QMainWindow(parent) {

    w = new QWidget(parent);
    layout = new QVBoxLayout();

    QHBoxLayout *head = new QHBoxLayout();

    QPushButton *back_btn = new QPushButton("Vrátit se");
    back_btn->setStyleSheet("font-size: 20px;");
    head->addWidget(back_btn,0, Qt::AlignLeft);

    QLabel *lbl = new QLabel("RECEPTY");
    lbl->setStyleSheet("color: green; font-size: 30px;");
    head->addWidget(lbl);

    QScrollArea *scroll = new QScrollArea();
    QWidget *recipes_w = new QWidget();
    recipes_list = new QVBoxLayout();

    stack = new QStackedWidget();
    stack->addWidget(w);

    prepareRecipes();

    recipes_w->setLayout(recipes_list);
    scroll->setWidget(recipes_w);

    layout->addLayout(head);
    layout->addWidget(scroll);

    layout->setAlignment(Qt::AlignTop);

    w->setLayout(layout);
    setCentralWidget(stack);

    connect(back_btn, &QPushButton::clicked, this, &Recipes::exitSlot);
    connect(this, &Recipes::selectRecipe, this, &Recipes::openRecipe);
}

Recipes::~Recipes() {

}

void Recipes::exitSlot() {
    emit Recipes::backToMenu();
}


void Recipes::openRecipe(int idx) {
    stack->setCurrentWidget(all_recipes_inst[idx]);
}

void Recipes::returnSlot() {
    stack->setCurrentWidget(w);
}

void Recipes::prepareRecipes() {
    QSqlQuery db_select("SELECT * FROM recipes;");
    int i = 0;
    while (db_select.next())  {
        QString recipe_name = db_select.value(0).toString();
        QString recipe_ings = db_select.value(1).toString();
        QString recipe_instr = db_select.value(2).toString();
        QImage *recipe_img = new QImage("images/" + recipe_name + ".jpg");

        QPushButton *button = new QPushButton(recipe_name);
        button->setStyleSheet("border: 1px solid black; font-size: 20px; background: white");
        button->setMinimumWidth(500);
        recipes_list->addWidget(button);

        RecipeWindow *cur_recipe =  new RecipeWindow(this, recipe_name, recipe_ings, recipe_instr, recipe_img);
        all_recipes_inst.push_back(cur_recipe);

        connect(button, &QPushButton::clicked, this, [this, i]() {
            Recipes::selectRecipe(i);
        });
        connect(all_recipes_inst[i], &RecipeWindow::backToRecipes, this, &Recipes::returnSlot);

        stack->addWidget(all_recipes_inst[i]);
        i++;
    }


}
